<img src="public/hasocket.png" width="100">

# 🔥**HAWebsocketIO Chat simple app**🔥



> STDid: 518H0469 || 518H0185>
@TDT-University
Major: Software engineer
Author: "Van Anh , Luu Hung" ||Teacher: "Dang Minh Thang"

<p align="right">
  <a href="https://nodejs.org/">
    <img
      alt="Node.js"
      src="https://nodejs.org/static/images/logo-light.svg"
      width="100"
    />
  </a>
</p>

--------------------------------------------------------
HOW TO RUN FILE
--------------------------------------------------------

```
compile in cmd 
    [+] npm install express
    [+] npm install nodemon -D
    [+] npm install socket.io
    [+] npm run dev
    [+] Go to browser link : localhost:3000
```
--------------------------------------------------------
WHAT IS WEBSOCKET?
--------------------------------------------------------
WebSoket is a technology that supports bidirectional communication between client and server by using a TCP socket to create an efficient and inexpensive connection. Although designed to be used exclusively for web applications, programmers can still incorporate them into any kind of application.

<img src="public/websocket.jpg" width="250">

**+Advantages**

WebSockets provides powerful bidirectional communication, low latency, and easy error handling. There is no need for as many connections as the Comet long-polling method and there are no downsides like Comet streaming. The API is also very easy to use directly without any additional layers, compared to Comet, which often requires a good library to handle reconnections, timeouts, Ajax requests (requires Ajax). , various acknowledgments and transport options (Ajax long-polling and jsonp polling).

**+Defect**

Disadvantages of WebSockets include:

There is no scope of requirement. Since WebSocket is a TCP socket and not an HTTP request, request-scoped services are not easy to use, like Hibernate's SessionInViewFilter. Hibernate is a classic framework that provides a filter around an HTTP request. When the request starts, it will set up a contest (containing transactions and JDBC link) bound to the request thread. When that request ends, the filter cancels this contest.

--------------------------------------------------------
WHAT IS NODE.JS?
--------------------------------------------------------
Node.js is a part system designed for writing scalable internet applications, especially web server. The program is written in JavaScript, carefully utilizing event-driven control, asynchronous input / output to minimize overhead and maximize scalability. Node.js includes Google's V8 JavaScript engine, libUV, and several other libraries.

Advantages of Node.js for web-socket programming:

**First:** 
javascript is an event-oriented programming language, in real-time programming, the event-programming approach is the wisest approach.

**Second:** 
Node.js runs non-blocking so the system doesn't have to pause to finish processing a request will help the server respond to the client almost immediately.

**Third:** 
socket programming requires you to build a two-way listening-response model. In other words, the role of the client and the server must be similar, but the client is running with javascript, so if the server is also running with javascript, then the programming will be easier and more user-friendly.

-------------------------------------------------------------
THANKS FOR READING! 👍 
-------------------------------------------------------------
CONTACT ME: [VĂN ANH](https://www.facebook.com/vananhdesigner/)
        OR: [LƯU HÙNG](https://www.facebook.com/kendy.luuu)

